if status is-interactive
    direnv hook fish | source

    function e
        emacsclient --no-wait $argv
    end

    function __fish_git_prompt_ready
        test "$(git rev-parse --show-toplevel 2>/dev/null)" != "$HOME"
    end

    set -g __fish_git_prompt_show_informative_status 1
    set -g __fish_git_prompt_showcolorhints 1

    set fish_color_user magenta

    set -U fish_greeting
end

set -gx EDITOR emacsclient
fish_add_path $HOME/.local/bin/

# Make fish behave more like emacs:
bind \eb backward-bigword
bind \ef forward-bigword
bind \el downcase-word
