# If not running interactively, don't do anything more
[ -z "$PS1" ] && return

# Download .compile-python.sh https://git.afpy.org/mdk/compile-python/raw/branch/main/compile-python.sh
# Download .python-prompt.sh https://git.afpy.org/mdk/python-prompt/raw/branch/main/python-prompt.sh

DEBFULLNAME="Julien Palard"
DEBEMAIL=${DEBFULLNAME/ /@}.fr
DEBMAIL=${DEBMAIL@L}

shopt -s cdspell
shopt -s dirspell
shopt -s autocd
shopt -s globstar
shopt -s nocaseglob
shopt -s histappend

bind 'set completion-ignore-case On'
bind 'set completion-map-case On'
bind 'set mark-symlinked-directories On'
bind 'set show-all-if-ambiguous On'
bind 'set colored-completion-prefix On'
bind 'set colored-stats On'
bind 'set show-all-if-unmodified On'
bind 'set visible-stats On'

bind '"\C-p":history-search-backward'
bind '"\C-n":history-search-forward'

umask 022

CDPATH="~/src/"
HISTSIZE=500000
HISTFILESIZE=100000
HISTCONTROL=ignoredups
HISTTIMEFORMAT='%F %T '


# http://nion.modprobe.de/blog/archives/572-less-colors-for-man-pages.html
export LESS_TERMCAP_mb=$'\E[01;31m'    # debut de blink
export LESS_TERMCAP_md=$'\E[01;31m'    # debut de gras
export LESS_TERMCAP_me=$'\E[0m'        # fin
export LESS_TERMCAP_so=$'\E[01;44;33m' # début de la ligne d'état
export LESS_TERMCAP_se=$'\E[0m'        # fin
export LESS_TERMCAP_us=$'\E[01;32m'    # début de souligné
export LESS_TERMCAP_ue=$'\E[0m'        # fin
export LC_ALL='fr_FR.UTF-8'
export PAGER='less -S'
export DEBEMAIL DEBFULLNAME
export EDITOR=emacs
export PIP_REQUIRE_VIRTUALENV=1
# needs: apt install lesspipe source-highlight
export LESSOPEN="| /usr/share/source-highlight/src-hilite-lesspipe.sh %s"  # see lesspipe(1)
export LESS=' -R '
export PYTHONDEVMODE=y

eval "`dircolors`"

alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias l='ls -lah --color=auto'
alias fingerprint='find /etc/ssh -name "*.pub" -exec ssh-keygen -l -f {} \;'
alias rekey='ssh-add -e /usr/lib/x86_64-linux-gnu/opensc-pkcs11.so >/dev/null 2>&1; ssh-add -s /usr/lib/x86_64-linux-gnu/opensc-pkcs11.so'

while read -r _ _ dest src  # costs 9ms
do
   if [[ (! -f "$HOME/$dest") || $(( RANDOM % 100 )) == 0 ]]
   then
       wget -q -O "$HOME/$dest" "$src"
   fi
   if [[ "$dest" == *".sh" ]]
   then
       . "$HOME/$dest"
   fi
done < <(grep '^# Download ' $HOME/.bashrc)
unset dest src

. /etc/bash_completion  # costs 6ms

if [[ "$TERM" != 'dumb' ]]
then
    _TITLE="\[\e]0;\H \W\a\]"
else
    _TITLE=''
fi

_PREV_FAIL="\`PREV_FAIL=\$?; if [ \$PREV_FAIL != 0 ]; then echo \[\e[31m\]\$PREV_FAIL \[\e[0m\]; fi\`"

git_prompt()
{
    # Wrapper around __git_ps1
    # to not show it when the repo is my dotfiles.
    if [[ "$(git rev-parse --show-toplevel 2>/dev/null)" != "$HOME" ]]
    then
        __git_ps1 "$1"
    fi
}

GIT_RED_FG='\e[38;2;244;77;39m'
GIT_RED_BG='\e[48;2;244;77;39m'
PY_BLUE_FG='\e[38;2;53;112;160m'
PY_BLUE_BG='\e[48;2;53;112;160m'
PY_YELLOW_FG='\e[38;2;255;222;87m'
HOSTNAME_COLOR=$'\E[1;34m'
USERNAME_COLOR=$'\E[1;35m'
PY_PS1='$(python_prompt " ${PY_BLUE_FG}🭮${PY_BLUE_BG}${PY_YELLOW_FG} %s \e[0m${PY_BLUE_FG}🭬\e[0m")'
GIT_PS1='$(git_prompt " ${GIT_RED_FG}🭮${GIT_RED_BG}\e[97m %s \e[0m${GIT_RED_FG}🭬\e[0m")'
PS1="${_TITLE}${_PREV_FAIL}${USERNAME_COLOR}\u\e[0m@${HOSTNAME_COLOR}\H\e[0m:\e[32m\w\e[0m${PY_PS1}${GIT_PS1}\n\$ "

if hash direnv &>/dev/null; then
    eval "$(direnv hook bash)"
fi

e()
{
    emacsclient --no-wait "$@"
}
