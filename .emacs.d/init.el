;; Made by Julien Palard <julien@palard.fr>
;;
;; Started on  Sun Nov 16 12:00:18 2008 Julien Palard
;;
;; To gather tags, according to emacs doc:
;;
;;    find . -name "*.[chCH]" -print | etags -
;;
;; The python mode, flycheck, and so requires:
;;
;;    pip install mypy pylint isort black pyflakes
;;
;; C-c TAB f    to fix imports
;; C-c C-;      to comment a line or region
;; C-c C-z      switch to shell


(setq user-full-name "Julien Palard"
      user-mail-address "julien@palard.fr")
(require 'use-package)
(require 'package)
(package-initialize)
(server-start)

(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("melpa" . "https://melpa.org/packages/")))

;; If there are no archived package contents, refresh them
(when (not package-archive-contents)
  (package-refresh-contents))

(use-package yasnippet
  :ensure t
  :config
  (yas-global-mode 1)
)

(use-package yasnippet-snippets
  :ensure t
)

(use-package company
  :ensure t
)

(use-package pkg-info  ;; Used by lsp-ui and flycheck
  :ensure t
)

(use-package vterm
  :config
  (setq vterm-max-scrollback 10000)
  ;(define-key vterm-mode-map (kbd "C-y") 'vterm--self-insert)
  ;(define-key vterm-mode-map (kbd "M-y") 'vterm--self-insert)
  (defun vterm-send-C-k ()
    "Send `C-k' to libvterm."
    (interactive)
    (kill-ring-save (point) (vterm-end-of-line))
    (vterm-send-key "k" nil nil t))
  (define-key vterm-mode-map (kbd "C-k") 'vterm-send-C-k)
)

(use-package ox-latex
  :config
  (add-to-list 'org-latex-classes
               '("lettre"
                 "\\documentclass[french,a4paper]{lettre}"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")))
)

(use-package envrc
  :ensure t
  :init (envrc-global-mode))

(use-package flycheck-aspell
  :ensure t
  :after (flycheck)
  :config
  (add-to-list 'flycheck-checkers 'markdown-aspell-dynamic)
;;  :hook (((markdown-mode) . flycheck-aspell)
;;         )

  :init
  (defun fd-switch-dictionary()
    (interactive)
    (let* ((dic ispell-current-dictionary)
      (change (if (string= dic "french") "english" "french")))
        (ispell-change-dictionary change)
        (message "Dictionary switched from %s to %s" dic change)))
  (global-set-key (kbd "C-c ! f") 'fd-switch-dictionary)
  )

(use-package flycheck-grammalecte
  :ensure t
  :after (flycheck-aspell)
  :config
  (flycheck-grammalecte-setup)
  (flycheck-add-next-checker 'markdown-aspell-dynamic 'grammalecte)
  (setq flycheck-global-modes '(not org-mode))
)

(use-package diminish
  :ensure t)

(use-package magit
  :ensure t)

(tool-bar-mode -1)

(use-package which-key
  :ensure t
  :config
  (which-key-mode)
)

(use-package lsp-mode
  :ensure t
  :init
  (setq lsp-keymap-prefix "C-c p")
  :custom
  (lsp-jedi-hover-disable-keyword-all t)
  (lsp-signature-doc-lines 1)
  (lsp-diagnostics-provider :none)
  (lsp-jedi-pylsp-extra-paths [])
  (lsp-inlay-hint-enable t)
  :config
  (set-face-attribute 'lsp-face-highlight-textual nil
                      :background "#666" :foreground "#ffffff"
                      )
  :hook (((python-mode) . lsp-deferred)
         ((rust-mode) . lsp)
         (lsp-enable-which-key-integration))
  :commands (lsp lsp-deferred)
)

(use-package python
  :custom
  (python-indent-guess-indent-offset nil)
)

(use-package lsp-ui
  :ensure t)

(use-package lsp-jedi
  :ensure t
  :after lsp-mode
  :config
  (with-eval-after-load "lsp-mode"
    (add-to-list 'lsp-disabled-clients 'pyls)
))

;; Test using flycheck-verify-setup
(use-package flycheck
  :ensure t
  :after lsp-mode
  :config
  (global-flycheck-mode t))

(use-package blacken
  :ensure t
  :commands (blacken-mode)
  :hook (python-mode . blacken-mode)
  :config
  (setq blacken-only-if-project-is-blackened t))

(use-package org-bullets
   :ensure t
   :init (add-hook 'org-mode-hook 'org-bullets-mode))

(use-package lorem-ipsum
  :ensure t
)

(use-package gnuplot-mode
  :ensure t
)

(use-package rust-mode
  :ensure t
)

(use-package po-mode
  :ensure t
)

(use-package yaml-mode
  :ensure t
)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)))

(add-hook 'org-mode-hook
          (lambda() (setq header-line-format
                                 '(:eval (org-display-outline-path nil t " > " t)))))

(defun my/org-agenda-process-dates-in-tables ()
  (goto-char (point-min))
  (while (not (eobp))
    (message "line: %s" (thing-at-point 'line t))
    (let* ((props (text-properties-at (point)))
           (datestr (plist-get props 'dotime)))
      (message "datestr: %s" datestr)
      (when (and datestr (string= (plist-get props 'org-category) "julien"))
        (let ((marker (plist-get props 'org-marker))
              (bound (point))
               text)
          (message "marker: %s" marker)
          (with-current-buffer (marker-buffer marker)
            (goto-char (marker-position marker))
            ;; we should now be at the date in the originating file
            ;; check that the date occurs in a table
            (when (org-at-table-p)
              (message "In a table")
               ;; convert the table to lisp structure and get the required row
              (let* ((table (delq 'hline (org-table-to-lisp)))
                     (row-index (org-table-current-line))
                     (row (nth (1- row-index) table)))
                (message "row: %s" (nth 0 row))
                ;; check that the date matches the field in the first column
                ;; - note that nth counts from 0.
                (when (string= (nth 0 row) datestr)
                  ;; select the field in the second column
                  (message "Dates are equal, nice!")
                  (setq text (nth 1 row))))))
          (when text
            (message "Will insert %s" text)
            (message "at end of line: %s" (thing-at-point 'line t))
            (end-of-line)
            ;; attempt to deal with tags - this is *NOT* robust
            (when (and (eq (char-before) ?:)
                       (search-backward " :" bound t))
              (while (looking-at " ")
                (backward-char))
              (forward-char)
              ;; +3 for the space and the two parens - see below
              (delete-char (+ 3 (length text))))
            (insert (concat " (" text ")"))))))

    (forward-line 1)))


(add-hook 'org-agenda-finalize-hook #'my/org-agenda-process-dates-in-tables)


;; Disable transient mark mode, I don't like it:
(transient-mark-mode nil)

;; Coding style
(setq-default indent-tabs-mode nil
              tab-width 4
              py-indent-offset 4
              )

;; Don't show trailing whitespaces in term-mode
(add-hook 'term-mode-hook
      (lambda() (make-local-variable 'show-trailing-whitespace)
        (setq show-trailing-whitespace nil)))

(global-font-lock-mode t)
(column-number-mode t)
(show-paren-mode t)

(global-set-key "\C-cc" 'compile)
(global-set-key "\M-n" 'forward-paragraph)
(global-set-key "\M-p" 'backward-paragraph)
(global-set-key "\C-xrv" 'list-registers)
(global-set-key (kbd "M-h") 'backward-kill-word)
(global-set-key "\C-cj" 'windmove-left)
(global-set-key "\C-ck" 'windmove-down)
(global-set-key "\C-cl" 'windmove-up)
(global-set-key "\C-c;" 'windmove-right)
(global-set-key "\C-x\M-%" 'query-replace-regexp) ;; As C-M-% is ~impossible to type in a terminal emulator:
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-switchb)
(global-set-key "\C-\M-v" 'clipboard-yank)

(add-hook 'before-save-hook 'delete-trailing-whitespace)

(setq version-control t  ; (for backup files)
      mouse-yank-at-point t
      inhibit-startup-message t
      make-backup-files t
      backup-directory-alist (quote ((".*" . "~/.emacs.d/backup/")))
      )

;; Save all backup file in this directory.
(setq-default delete-old-versions t)

(fset 'yes-or-no-p 'y-or-n-p)

(setq-default truncate-partial-width-windows nil)

(menu-bar-mode -1)

(use-package python-isort
  :ensure t
  :hook ((python-mode) . python-isort-on-save-mode)
  :config
  (setq python-isort-arguments '("--stdout" "--atomic" "-" "--profile=black"))
)

(use-package whitespace
  :diminish (whitespace-mode global-whitespace-mode whitespace-newline-mode)
  :hook ((python-mode) . whitespace-mode)
  :config
  (setq show-trailing-whitespace t)
  (setq whitespace-line-column 88)
  (setq whitespace-style '(face empty tabs lines-tail trailing))
)

(use-package spacemacs-theme
  :defer t
  :config
  ;(enable-theme 'spacemacs-light)
  :init
  (load-theme 'spacemacs-light t)
  (set-fontset-font t nil "Unifont Upper" nil 'append)  ; For 🭮 and 🭬

)

;; hex color
(defvar hexcolour-keywords
  '(("#[a-fA-F[:digit:]]\\{3,6\\}"
     (0 (let ((colour (match-string-no-properties 0)))
          (if (or (= (length colour) 4)
                  (= (length colour) 7))
              (put-text-property
               (match-beginning 0)
               (match-end 0)
               'face (list :background (match-string-no-properties 0)
                           :foreground (if (>= (apply '+ (x-color-values
                                                          (match-string-no-properties 0)))
                                               (* (apply '+ (x-color-values "white")) .6))
                                           "black" ;; light bg, dark text
                                         "white" ;; dark bg, light text
                                         )))))
        append))))


(defun hexcolour-add-to-font-lock ()
  (interactive)
  (font-lock-add-keywords nil hexcolour-keywords t))

(add-hook 'css-mode-hook 'hexcolour-add-to-font-lock)
(add-hook 'sass-mode-hook 'hexcolour-add-to-font-lock)
(add-hook 'emacs-lisp-mode-hook 'hexcolour-add-to-font-lock)
(add-hook 'conf-xdefaults-mode-hook 'hexcolour-add-to-font-lock)
(add-hook 'before-save-hook 'delete-trailing-whitespace)

(setq org-directory "~/Org")
(setq org-default-notes-file "~/Org/julien.org")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auth-source-save-behavior nil)
 '(c-basic-offset 4)
 '(css-indent-offset 2)
 '(custom-safe-themes
   '("bbb13492a15c3258f29c21d251da1e62f1abb8bbd492386a673dcfab474186af" default))
 '(fido-mode t)
 '(icomplete-vertical-mode t)
 '(message-send-mail-function 'message-send-mail-with-sendmail)
 '(org-agenda-files '("~/Org/julien.org"))
 '(org-agenda-span 'fortnight)
 '(org-modules
   '(ol-bbdb ol-bibtex ol-docview ol-doi ol-eww ol-gnus org-habit ol-info ol-irc ol-mhe ol-rmail ol-w3m))
 '(scroll-bar-mode nil)
 '(send-mail-function 'mailclient-send-it)
 '(visible-bell t))

(setq rust-format-on-save t)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


;; For rust need : apt install rust-src rustup
;; $ rustup default stable
;; $ rustup component add rust-analyzer
