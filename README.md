# Install a new Debian

On a laptop, I typically use
https://cdimage.debian.org/images/unofficial/non-free/images-including-firmware/
to get the Wi-Fi firmwares.


# Pin default release

    echo 'APT::Default-Release "bookworm";' > /etc/apt/apt.conf.d/00default

Then add sid (for firefox, at least):

    deb https://deb.debian.org/debian sid main
    deb-src https://deb.debian.org/debian sid main


# Install my dotfiles

I clone the repo in `~/` thanks to a `.gitignore` containing `*`, so
the first setup looks like:

    git init
    git fetch git@git.afpy.org:mdk/dotfiles.git
    git checkout -f main


## Packages I may need

```
apt install firmware-iwlwifi
apt install firmware-nonfree
apt install tlp  # Optimize Laptop Battery Life
apt install opensc opensc-pkcs11  # For yubikey
```

With:

    echo'CPU_ENERGY_PERF_POLICY_ON_BAT=power > /etc/tlp.d/50-cpu.conf


## Packages I don't want

```
aptitude purge ttf-bitstream-vera
```

see https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=981577 or simply `echo $'e\xcc\x81a'`



# What I typically do on a new laptop

- Change grub sleep time in `/etc/default/grub` (and run `update-grub`)
- Set `Option "Tapping" "on"` for touchpad in `/usr/share/X11/xorg.conf.d/40-libinput.conf`
- (May have to change button map, like `xinput set-button-map 12 1 2 2 4 5 6 7`, use `xev` from `x11-utils` to see button ids).
- https://wiki.debian.org/TransparentEncryptionForHomeFolder
- Storage=volatile dans /etc/systemd/journald.conf


# Firefox extensions

- [uBlock in medium mode](https://github.com/gorhill/uBlock/wiki/Blocking-mode)
- Multi Account Containers
- Grammalecte
