#!/usr/bin/env python3.12
import sys
from pathlib import Path
import shutil


def find(path, maxdepth=3, prune=()):
    for child in path.iterdir():
        if child.name in prune:
            continue
        yield child
        if child.is_dir() and maxdepth > 0:
            yield from find(child, prune=prune, maxdepth=maxdepth - 1)


def rm(path):
    print(f"rm -fr {path}")
    if "-n" in sys.argv:
        return
    if path.is_file():
        path.unlink()
    else:
        shutil.rmtree(path)


PRUNE = {"backup", "auto-save-list"}
TO_DROP = {Path(".tox"), Path(".mypy_cache"), Path(".envrc"), Path(".venv"), Path("*~"), Path("#*#")}

for file in find(Path("."), prune=PRUNE):
    if any(file.match(pat) for pat in TO_DROP):
        rm(file)
