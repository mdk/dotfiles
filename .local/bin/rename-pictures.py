#!/usr/bin/env python3

"""Rename jpeg according to their EXIF date."""

import logging
from argparse import ArgumentParser
import struct
import datetime as dt
from itertools import count
from pathlib import Path

import exifread  # sudo apt install python3-exifread
from pymediainfo import MediaInfo  # sudo apt install python3-pymediainfo


def parse_args():
    """Parses command line arguments."""
    parser = ArgumentParser(description="Sort jpeg according to their EXIF date.")
    parser.add_argument(
        "--verbose",
        "-v",
        action="store_const",
        const=logging.DEBUG,
        default=logging.INFO,
        dest="loglevel",
    )
    parser.add_argument(
        "-n",
        "--no",
        "--dry-run",
        dest="dry_run",
        action="store_true",
        default=False,
        help="Dry run: Nothing is actually done: "
        "show what files would have been moved.",
    )
    parser.add_argument(
        "sources",
        nargs="*",
        help="file or directory to work on",
        type=Path,
        default=(Path("."),),
    )
    return parser.parse_args()


def suffixes():
    yield ""
    for i in count(1):
        yield f"-{i}"


def extract_date(file):
    if file.suffix.lower() in (".jpg", ".jpeg"):
        return extract_jpg_date(file)
    elif file.suffix.lower() == ".mov":
        return extract_mov_date(file)
    else:
        raise ValueError(f"Not a jpg or mov file: {file}")


def extract_mov_date(file):
    media_info = MediaInfo.parse(file)
    general_track = media_info.general_tracks[0]
    return parse_date(general_track.encoded_date)


def extract_jpg_date(file) -> dt.datetime|None:
    exif_tag_to_search = [
        "EXIF DateTimeOriginal",
        "Image DateTimeOriginal",
        "EXIF DateTimeDigitized",
    ]
    logging.debug("Opening %s", file)
    tags = {}
    with open(file, "rb") as jpg_opened_file:
        try:
            tags = exifread.process_file(jpg_opened_file)
        except (UnicodeDecodeError, struct.error):
            logging.exception("Exception while parsing exim informations")
            return None

    logging.debug("Found tags: %s", ", ".join(name for name in list(tags.keys())))
    for tag in exif_tag_to_search:
        if tag in tags:
            try:
                return parse_date(str(tags[tag]))
            except ValueError:
                continue
    return dt.datetime.fromtimestamp(file.stat().st_mtime, tz=dt.timezone.utc)


def rename_file_with_date(source: Path, date, dry_run=False):
    ext = source.suffix.lower()
    if ext == ".jpeg":
        ext = ".jpg"
    date = date.strftime("%Y-%m-%d %H:%M:%S")
    for suffix in suffixes():
        dest = source.parent / f"{date}{suffix}{ext}"
        if source.name == dest.name:
            logging.debug("%s already correctly named", source.name)
            return
        if dest.is_file():
            continue
        if dry_run:
            logging.info("mv %s %s", source, dest)
        else:
            source.rename(dest)
        return


def parse_date(date: str) -> dt.datetime|None:
    for date_format in ("%Y:%m:%d %H:%M:%S", "%Y-%m-%d %H:%M:%S UTC"):
        try:
            found = dt.datetime.strptime(date, date_format)
            if dt.datetime(2000, 1, 1, 0) < found < dt.datetime(2000, 1, 1, 1):
                raise ValueError("Pas crédible.")
            return found
        except ValueError:
            continue
    logging.error("Cannot parse date %s", date)
    raise ValueError("Cannot parse date")


def rename_file(file: Path, dry_run=True):
    if date := extract_date(file):
        rename_file_with_date(file, date, dry_run)


def recursively_rename_files(directory: Path, dry_run=True):
    logging.debug("Scanning %s", directory)
    for file in directory.rglob("*.*"):
        if file.suffix.lower() in {".mov", ".jpg", ".jpeg"}:
            rename_file(file, dry_run)


def rename_file_or_directory(file_or_directory: Path, dry_run=True):
    if file_or_directory.is_file():
        rename_file(file_or_directory, dry_run)
    else:
        recursively_rename_files(file_or_directory, dry_run)


def main():
    args = parse_args()
    logging.basicConfig(level=args.loglevel)
    for source in args.sources:
        rename_file_or_directory(source, args.dry_run)


if __name__ == "__main__":
    main()
